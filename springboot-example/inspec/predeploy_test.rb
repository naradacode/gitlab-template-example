title 'Section Pre Deployment Test'

DB_IP = input('DB_IP')
DB_PORT = input('DB_PORT')
TOMCAT_OS_USER = input('TOMCAT_OS_USER')
TOMCAT_OS_GROUP = input('TOMCAT_OS_GROUP')
TOMCAT_HTTP_PORT = input('TOMCAT_HTTP_PORT')
TOMCAT_BIN = input('TOMCAT_BIN')
TARGET_ENV = input('TARGET_ENV')
APP_CONF_PATH = input('APP_CONF_PATH')
APP_JAVA_OPTS = input('APP_JAVA_OPTS')

control 'predeploy.server.access.db' do
    impact 1.0
    title 'Check network access to postgres database'
    desc 'Check network access to postgres database from tomcat'

    describe host(DB_IP, port: DB_PORT, protocol: 'tcp') do
      it { should be_reachable }
      it { should be_resolvable }
    end
end

control 'predeploy.tomcat.conf.files' do
  impact 1.0
  title 'Check for existence and correct permissions of tomcat files'
  desc 'Check for existence and correct permissions of tomcat files: setenv.sh'

  describe file(TOMCAT_BIN + '/setenv.sh') do
    it { should exist }
    it { should be_file }
    its('owner') { should eq input('TOMCAT_OS_USER') }
    its('group') { should eq input('TOMCAT_OS_GROUP') }
    its('mode') { should cmp '0775' }
  end
end

control 'predeploy.tomcat.conf.setenv' do
  impact 1.0
  title 'Check env variables setting'
  desc 'Check env variables setting: env, path, JAVA_OPTS'

  describe file(TOMCAT_BIN + '/setenv.sh') do
    its('content') { should match 'export env=' + TARGET_ENV }
    its('content') { should match 'export path=' + APP_CONF_PATH }
    its('content') { should match 'export JAVA_OPTS="\$JAVA_OPTS ' + APP_JAVA_OPTS + '"' }
  end
end
title 'Section Post Deployment Test'

TOMCAT_IP = input('TOMCAT_IP')
TOMCAT_HTTP_PORT = input('TOMCAT_HTTP_PORT')

control 'postdeploy.tomcat.ports.listening' do
    impact 1.0
    title 'Check listening http port for Apache Tomcat'
    desc 'Apache Tomcat should listen http port on port ' + TOMCAT_HTTP_PORT

    describe port(TOMCAT_HTTP_PORT) do
      it { should be_listening }
      its('protocols') { should include 'tcp6' }
    end
end

control 'postdeploy.tomcat.access.inbound' do
    impact 1.0
    title 'Check inbound connection using private ip for Apache Tomcat'
    desc 'Apache Tomcat should be reachable and resolvable on port ' + TOMCAT_HTTP_PORT + 'using private ip ' + TOMCAT_IP

    describe host(TOMCAT_IP, port: TOMCAT_HTTP_PORT, protocol: 'tcp') do
      it { should be_reachable }
      it { should be_resolvable }
    end
end
